<div id="top"></div>

<div align="center">
    <a href="https://gitlab.com/dobodev/user-administration/README.md">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">User Administration</h3>

<!-- TABLE OF CONTENTS -->
<details open>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>

<!-- ABOUT THE PROJECT -->
## About The Project
![home](images/screenshots/home.png)

![forgot2](images/screenshots/forgot2.png)

![like1](images/screenshots/like1.png)

![like5](images/screenshots/like5.png)

![login5](images/screenshots/login5.png)

![register2](images/screenshots/register2.png)

- **Web App for Users Management**
- **Storing data in a database**
- **Users CRUD operations via AJAX requests**
- **Users profile updates, avatar images**
- **Users Administration via Admin**
- **Email confirmation on registration, forgot password functionality**
- **Vote system for users**
- **Vote system for registered users**
- **Top Users accordion menu**

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- BULIT WITH -->
### Built-With
* [![PHP][PHP.net]][PHP-url]
* [![MySQL][MySQL.com]][MySQL-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![Javascript][JavaScript.org]][JavaScript-url]
* [![JQuery][JQuery.com]][JQuery-url]
* [![Apache][Apache.org]][Apache-url]
* [![CSS][CSS.org]][CSS-url]
* [![HTML][HTML.org]][HTML-url]

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

<p align="right">(<a href="#top">back to top</a>)</p>


### Installation
1. **Clone the repository**: into your local machine server directory /user-administration
- [git clone https://gitlab.com/dobodev/user-administration.git](https://gitlab.com/dobodev/user-administration.git)
2. **Navigate to the project directory**:
- cd user-administration
3. Rename the file [.env.example] => [.env]
4. Set the correct environment variables into the [.env] file

### localhost || VM
**Database Setup**:
- import database dump from *_mysql/* folder

**Install Prerequisites**
- cd project root/ =>  composer dump-autolad, so we can use the composer's autoloader for our classes
- cd project root/ => composer update to install required dependencies

**Serve the app**
-- Run your local server in the app root directory

### Dockerize (assuming the host machine has installed Docker Engine && || Docker Desktop)
- **docker compose up --build**
- *(default run setup): go to  http://localhost:8008/*

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->
## Acknowledgments
* <strong></strong>

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->
## Contact
LinkedIn - [https://www.linkedin.com/in/doychin-borisov/](https://www.linkedin.com/in/doychin-borisov/)

### Built With
<!-- MARKDOWN LINKS & IMAGES -->
[PHP.net]: https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white
[PHP-url]: https://www.php.net/
[MySQL.com]: https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white
[MySQL-url]: https://www.mysql.com/
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com
[JavaScript.org]: https://img.shields.io/badge/-JavaScript-F7DF1E?style=flat-square&logo=javascript&logoColor=black
[JavaScript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[Apache.org]: https://img.shields.io/badge/apache-%23D42029.svg?style=for-the-badge&logo=apache&logoColor=white
[Apache-url]: https://httpd.apache.org
[CSS.org]: https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white
[CSS-url]: https://developer.mozilla.org/en-US/docs/Web/CSS
[HTML.org]: https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white
[HTML-url]: https://developer.mozilla.org/en-US/docs/Web/HTML

[linkedin-url]: https://www.linkedin.com/in/doychin-borisov/



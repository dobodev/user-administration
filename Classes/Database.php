<?php

namespace Classes;

use Classes\Validator;

class Database
{
    private string $driver;
    private string $host;
    private string $database_name;
    private string $port;
    private string $user;
    private string $password;
    private string $dsn;

    /** @var array<mixed> */
    private array $pdo_options;
    private string $users_table = 'users';
    private string $counter_table = 'user_counter';
    private \PDO $connection;

    public function __construct()
    {
        $this->driver = $_ENV['DB_CONNECTION_DRIVER']; // mysql
        $this->host = $_ENV['DB_HOST'];
        $this->database_name = $_ENV['DB_DATABASE'];
        $this->user = $_ENV['DB_USER'];
        $this->password = $_ENV['DB_PASSWORD'];
        $this->port = $_ENV['DB_PORT'];

        $this->dsn = $this->driver . ':dbname=' . $this->database_name .
            ';host=' . $this->host . ';port=' . $this->port . ';charset=utf8';

        $this->pdo_options = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        // Set the DB connection
        $this->setConnection();
    }

    /**
     * Set PDO connection
     */
    public function setConnection(): void
    {
        try {
            $this->connection = new \PDO($this->dsn, $this->user, $this->password, $this->pdo_options);
        } catch (\PDOException $e) {
            print "Database Connection Error: " . $e->getMessage();
            die();
        }
    }

    /**
     * Select user from the database, using an email as parameter. Returns user as AssocArray
     * @param string $email
     * @return array<mixed> array
    */
    public function selectUserFromDatabase(string $email): array
    {
        $sql = "SELECT * FROM $this->users_table WHERE email=:email";
        $result_sql = $this->connection->prepare($sql);
        $result_sql->execute([':email' => $email]);

        if ($result_sql->rowCount() <= 0) {
            return [];
        }
        return $result_sql->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Select user from the database, using firstName as parameter. Returns user as AssocArray
     * @param string $name
     * @return array<mixed> array
    */
    public function selectUserByName(string $name): array
    {
        $sql = "SELECT firstName, lastName, image FROM $this->users_table WHERE firstName=:name";
        $result_sql = $this->connection->prepare($sql);
        $result_sql->execute([':name' => $name]);

        return $result_sql->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Select User data from tbl-users and count likes from tbl-counter_table per user. Returns user data as AssocArray
     * @param bool $topUser
     * @return array<mixed> array
     *
    */
    public function selectUsersAll(?bool $topUser = false): array
    {
        // Joined result from the 2 db-tables
        if ($topUser === true) {
            $sql = "SELECT $this->users_table.firstName, $this->users_table.image, $this->counter_table.user_id,
				(
                    SELECT COUNT(*)
                    FROM $this->counter_table
                    WHERE $this->users_table.firstName = $this->counter_table.user_liked
                ) AS likes
				FROM $this->users_table
				LEFT JOIN $this->counter_table
				ON $this->users_table.id = $this->counter_table.user_id
				ORDER BY likes DESC
				LIMIT 1";

            $result_sql = $this->connection->query($sql, \PDO::FETCH_ASSOC);

            $users = [];
            foreach ($result_sql as $user) {
                $users[] = $user;
            }

            return $users;
        } else {
            $sql = "SELECT $this->users_table.firstName, $this->users_table.image, $this->counter_table.user_id,
				(
                    SELECT COUNT(*)
                    FROM $this->counter_table
                    WHERE $this->users_table.firstName = $this->counter_table.user_liked
                ) AS likes
				FROM $this->users_table
				LEFT JOIN $this->counter_table
				ON $this->users_table.id = $this->counter_table.user_id";

            // Return the count of the users
            // $sql = "SELECT user_liked, COUNT(*) FROM $this->counter_table GROUP BY user_liked";
            $result_sql = $this->connection->query($sql, \PDO::FETCH_ASSOC);

            $users = [];
            foreach ($result_sql as $user) {
                $users[] = $user;
            }
            return $users;
        }
    }

    /**
     * Check the user session in the DB
     * @param string $email
     * @param string $logged
     * @return array<mixed> array
    */
    public function checkUserLogged(string $email, string $logged): array
    {
        $sql = "SELECT id, firstName, email, logged FROM $this->users_table WHERE email=:email AND logged=:logged";
        $result_sql = $this->connection->prepare($sql);
        $result_sql->execute([':email' => $email, ':logged' => $logged]);

        return $result_sql->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Select user from the database, using an email as parameter. Returns user as AssocArray
     * @param string $reset_string
     * @return array<mixed> array
    */
    public function selectUserResetstring(string $reset_string): array
    {

        $pdo_query = "SELECT * FROM $this->users_table WHERE reset_string=:reset_string";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':reset_string' => $reset_string]);

        $q_res = $pdo_request->fetch(\PDO::FETCH_ASSOC);

        return $q_res;
    }

    /**
     * Check if user exists in the database, using an email param. Return true/false
     * @param string $email
    */
    public function emailExist(string $email): bool
    {
        $exist = false;
        $pdo_query = "SELECT * FROM $this->users_table WHERE email=:email";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':email' => $email]);

        $q_res = $pdo_request->fetch(\PDO::FETCH_ASSOC);

        if ($q_res) {
            $exist = true;
        }

        return $exist;
    }

    /**
     * Insert user into the database
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $password
     * @return array<mixed> array
    */
    public function insertUserDatabase(string $firstName, string $lastName, string $email, string $password): array
    {
        $pdo_query = "INSERT INTO $this->users_table (firstName, lastName, email, password, created_at, updated_at)
        VALUES(:firstName, :lastName, :email, :password, now(), now())";

        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([
            ':firstName' => $firstName,
            ':lastName' => $lastName,
            ':email' => $email,
            ':password' => $password
        ]);

        $q_res = $pdo_request->fetch(\PDO::FETCH_ASSOC);

        return $q_res;
    }

    /**
     * Insert user into the database
     * @param string $email
    */
    public function deleteUserDatabase(string $email): int
    {
        $pdo_query = "DELETE FROM $this->users_table WHERE email=:email_p";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':email_p' => $email]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

    /**
     * Update user into the database
     * @param string $firstName
     * @param string $lastName
     * @param string $email
    */
    public function updateUserDatabase(string $firstName, string $lastName, string $updated_at, string $email): int
    {
        $pdo_query = "UPDATE $this->users_table
                    SET firstName=:firstName, lastName=:lastName, updated_at=:updated_at
                    WHERE email=:email";

        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([
            ':firstName' => Validator::trim($firstName),
            ':lastName' => Validator::trim($lastName),
            ':updated_at' => Validator::trim($updated_at),
            ':email' => Validator::trim($email)
        ]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

    /**
     * Update user password into the database
     * @param string $pass
     * @param string $id
    */
    public function updateUserPassword(string $pass, string $id): int
    {

        $pdo_query = "UPDATE $this->users_table SET password=:pass WHERE id=:id";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':pass' => $pass, 'id' => $id]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

    /**
     * Update record into the database
     * @param string $email
     * @param string $logged_param
    */
    public function setUserLogged(string $email, string $logged_param): int
    {
        $pdo_query = "UPDATE $this->users_table SET logged=:logged_param WHERE email=:email_p";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':logged_param' => $logged_param, ':email_p' => $email]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

    /**
     * Update user reset_string field the database
     * @param string $reset_string
     * @param string $email
    */
    public function setUserResetString(string $reset_string, string $email): int
    {
        $pdo_query = "UPDATE $this->users_table SET reset_string=:reset_string WHERE email=:email";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':reset_string' => $reset_string, ':email' => $email]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

      /**
     * Save uploaded img to the database
     * @param string $img
     * @param string $email
    */
    public function saveImg(string $img, string $email): int
    {
        $pdo_query = "UPDATE $this->users_table SET image=:img WHERE email=:email";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':img' => $img, ':email' => $email]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

      /**
     * Save the liked user name data into the database
     * @param string $user_liked
     * @param string $visitor_id
    */
    public function likeUser($user_liked, $visitor_id): int
    {
        $user_reacted = "SELECT * FROM $this->counter_table WHERE user_id=:visitor_id";
        $user_reacted_q = $this->connection->prepare($user_reacted);
        $user_reacted_q->execute([':visitor_id' => $visitor_id]);

        if ($user_reacted_q->rowCount() == 0) {
            $pdo_query_ins = "INSERT INTO $this->counter_table (user_liked, user_id, created_at)
            VALUES(:user_liked, :visitor_id, now())";
            $pdo_request_ins = $this->connection->prepare($pdo_query_ins);
            $pdo_request_ins->execute([':user_liked' => $user_liked, ':visitor_id' => $visitor_id]);

            return $pdo_request_ins->rowCount();
        } else {
            $pdo_query_upd = "UPDATE $this->counter_table SET user_liked=:user_liked, created_at=now()
            WHERE user_id=:visitor_id";
            $pdo_request_upd = $this->connection->prepare($pdo_query_upd);
            $pdo_request_upd->execute(['
                :user_liked' => Validator::trim($user_liked),
                'visitor_id' => Validator::trim($visitor_id)
            ]);

            return $pdo_request_upd->rowCount();
        }
    }

    /**
     * Save the liked user name data into the database
     * @param string $user_liked
     * @param string $visitor_id
    */
    public function unLikeUser(string $user_liked, string $visitor_id): int
    {
        $pdo_query = "DELETE FROM $this->counter_table WHERE user_liked=:user_liked AND user_id=:visitor_id ";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute(['user_liked' => $user_liked, 'visitor_id' => $visitor_id]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

    /**
     * Check the user reaction for likes
     * @param string $logged_user_id
     * @return array<mixed> array
     */
    public function checkUserReaction(string $logged_user_id): array
    {
        $pdo_query = "SELECT user_liked FROM $this->counter_table WHERE user_id=:logged_user_id ";
        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':logged_user_id' => $logged_user_id ]);

        $result = $pdo_request->fetch(\PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * Update user name
     * @param string $name
     * @param string $email
     * @return int
    */
    public function updateName(string $name, string $email): int
    {
        $pdo_query = "UPDATE $this->users_table SET firstName=:name WHERE email=:email";

        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':name' => $name, ':email' => $email]);

        return $pdo_request->rowCount();
    }

    /**
     * Check if our database_schema && users_table exists into mysql information_schema
    */
    public function dataTableExist(): int
    {
        $pdo_query = "SELECT TABLE_NAME
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA =:database_name
        AND TABLE_NAME=:users_table";

        $pdo_request = $this->connection->prepare($pdo_query);
        $pdo_request->execute([':database_name' => $this->database_name, ':users_table' => $this->users_table]);

        $q_res = $pdo_request->rowCount();

        return $q_res;
    }

    /**
     * Create and initialize database table with admin inserted at creation
     * @return void
    */
    public function createDatabaseTable(): void
    {
        $adminPass = password_hash('pass', PASSWORD_DEFAULT);
        $sql = "CREATE TABLE users (
		  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		  firstName VARCHAR(40) NOT NULL,
		  lastName VARCHAR(40) NOT NULL,
		  email VARCHAR(40) NOT NULL,
		  logged VARCHAR(50),
		  password VARCHAR(99) NOT NULL,
		  reset_string VARCHAR(55),
		  image VARCHAR(100),
		  created_at VARCHAR(30),
		  updated_at VARCHAR(30)
	    );

        INSERT INTO users (firstname, lastName, email, password, created_at, updated_at)
            VALUES('Administrator', 'Administrator', 'admin@gmail.com', '" . $adminPass . "', now(), now())";

        $this->connection->exec($sql);
    }
}

<?php

namespace Classes;

use Classes\Database;

class User extends Database
{
    private string $firstName;
    private string $lastName;
    private string $email;
    private string $password;

    public function __construct(string $firstName, string $lastName, string $email, string $password)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        // $this->password = $password;
        $this->password = password_hash($password, PASSWORD_DEFAULT);

        parent::__construct();
    }

    /**
     * Get user record based on email
     * @param string $email
     * @return array<mixed> array
    */
    public function getUserRecord(string $email)
    {
        return $this->selectUserFromDatabase($email);
    }

    /**
     * Check user exists by email
     * @param string $email
     * @return bool
     */
    public function userExist(string $email): bool
    {
        return $this->emailExist($this->email);
    }

    /**
     * Insert user into the database
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $password
     * @return array<mixed> array
    */
    public function userCreate(string $firstName, string $lastName, string $email, string $password): array
    {
        return $this->insertUserDatabase($this->firstName, $this->lastName, $this->email, $this->password);
    }

    /**
     * Check if database exist
     * @return int
     */
    public function databaseExist(): int
    {
        return $this->dataTableExist();
    }

    /**
     * Summary of createDatabase
     * @return void
     */
    public function createDatabase(): void
    {
        $this->createDatabaseTable();
    }

    /**
     * Set user name
     * @param string $name
     * @param string $email
     * @return int
     */
    public function setName(string $name, string $email): int
    {
        $this->firstName = Validator::trim($name);

        $result = $this->updateName($this->firstName, $email);

        return $result;
    }
}

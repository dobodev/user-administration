<?php

namespace Classes;

class Validator
{
    /**
     * Validate email
     * @param string $email
     * @return mixed
     */
    public static function validateEmail(string $email): mixed
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Trim a string
     * @param string $data
     * @return string $data
     */
    public static function trim(string $data): string
    {
        $data = trim($data);
        $data = htmlspecialchars($data, ENT_QUOTES);
        return $data;
    }
}

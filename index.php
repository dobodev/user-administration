<?php

require __DIR__ . '/config/bootstrap.php';

use Classes\Database;

// Database Instance
$db = new Database();

$all_users = $db->selectUsersAll();

$top_user = $db->selectUsersAll($topUser = true);

if (isset($_SESSION['email'])) {
    $user_exist_db = $db->selectUserFromDatabase($_SESSION['email']);
    $logged_user = $db->checkUserLogged($_SESSION['email'], $_SESSION['logged']);
}

if (isset($_GET['url_action']) && !empty($_GET['url_action'])) {
    $url_action = $_GET['url_action'];

    $help_msg = '';
    switch ($url_action) {
        case 'error_incorrect_data':
            $help_msg = 'Enter your credentials to access that page!';
            break;
        case 'error_credentials':
            $help_msg = 'Please enter your email and password correctly';
            break;
        case 'restricted':
            $help_msg = 'Unauthorized Access';
            break;
        case 'logout':
            $help_msg = 'Successfully logged out';
            break;
        case 'incorrect_data':
            $help_msg = 'Please fill all the fields correctly';
            break;
        case 'user_exist':
            $help_msg = 'There is a registration with that email';
            break;
        case 'user_registered':
            $help_msg = 'Please login with your credentials';
            break;
        case 'error_exists':
            $help_msg = 'Email not found';
            break;
        case 'profile_deleted':
            $help_msg = 'The Profile has been successfully deleted';
            break;
        case 'profile_created':
            $help_msg = 'The Profile has been successfully created';
            break;
        case 'db_fail':
            $help_msg = 'Sorry. There is a problem with the database!';
            break;
        case 'message_sent':
            $help_msg = 'Message sent';
            break;
        case 'reset_password':
            $help_msg = 'Check your email, and reset the password';
            break;
        case 'mailNotFound':
            $help_msg = 'Not existing email, please check, or try login!';
            break;
        case 'password_changed':
            $help_msg = 'password_changed';
            break;
        default:
            $help_msg = '';
    }
}
?>

<!-- header -->
<?php require_once 'resources/views/header.php'; ?>

<?php require_once 'resources/views/navbar.php'; ?>

<?php require_once 'resources/views/content.php'; ?>

<!-- JS section -->
<script type="text/javascript">
    var helpMsg = '<?php echo isset($help_msg) ? $help_msg : ''; ?>';  // get the $_GET url-param
    var email_login_session = (sessionStorage.getItem('email_login') != 'undefined') ?
        sessionStorage.getItem('email_login') :
        '';
    var firstName_session = (sessionStorage.getItem('firstName') != 'undefined') ?
        sessionStorage.getItem('firstName') :
        '';
    var lastName_session = (sessionStorage.getItem('lastName') != 'undefined') ?
        sessionStorage.getItem('lastName') :
        '';
    var email_session = (sessionStorage.getItem('email_reg') != 'undefined') ? sessionStorage.getItem('email_reg') : '';

    var logged = '<?php echo ( isset($logged_user) ? json_encode($logged_user) : ''); ?>';
</script>

<!-- footer  -->
<?php require_once 'resources/views/footer.php'; ?>



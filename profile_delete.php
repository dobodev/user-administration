<?php

require __DIR__ . '/config/bootstrap.php';

use Classes\Database;

// Database Instance
$db = new Database();

$user_email = $_SESSION['email'] ?? 'no_email';
$user_firstName = $_SESSION['firstName'] ?? 'no_firstName';
$user_lastName = $_SESSION['lastName'] ?? 'no_lastName';
$user_logged = $_SESSION['logged'] ?? 'no_logged';

$user_exist = $db->selectUserFromDatabase($user_email);

// Compare the session vs DB record
if ($user_logged != $user_exist['logged']) {
    $login_error = 'hacking';
    header("Location: index.php?url_action=" . $login_error);
    session_unset();
    session_destroy();
    die('Unauthorized access');
} else {
    // Delete the database record
    $user_delete = $db->deleteUserDatabase($user_email);

    if ($user_delete) {
        session_unset();
        session_destroy();
        $login_error = 'profile_deleted';
        header("Location: index.php?url_action=" . $login_error . '#home-section');
        die('Profile Deleted');
    } else {
        $login_error = 'not_deleted';
        header("Location: profile.php?url_action=unableToDelete");
        die('Unable to delete user');
    }
}

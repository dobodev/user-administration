<?php

require __DIR__ . '/config/bootstrap.php';

use Classes\Database;

// Database Instance
$db = new Database();

$user_email = $_SESSION['email'] ?? '';
$user_firstName = $_SESSION['firstName'] ?? '';
$user_lastName = $_SESSION['lastName'] ?? '';
$user_logged = $_SESSION['logged'] ?? '';
$user_updated = $_SESSION['updated_at'] ?? '';

$user_exist_db = $db->selectUserFromDatabase($user_email);

// Compare the session vs DB record
if (
    $user_logged != $user_exist_db['logged'] ||
    $user_logged == null ||
    $user_exist_db == false
) {
    session_unset();
    session_destroy();
    $login_error = 'restricted';
    header("Location: index.php?url_action=" . $login_error . '#home-section');
    die('Unable to proceed');
}
?>

<?php require_once 'resources/views/profile/header.php'; ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
      //Home
    $('#homeButton').on('click', function(){
      window.location.href= "index.php";
    });
    //Exit
    $('#exitButton').on('click', function(){
      window.location.href= "logout.php";
    });
  </script>

  </body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Users Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/../public/fonts/icomoon/style.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="/../public/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="/../public/css/aos.css">
    <link rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    >
    <link rel="stylesheet" type="text/css" href="/../public/css/style.css">
    <link rel="icon" type="image/x-icon" href="/../public/images/logo.png">
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" id="home">


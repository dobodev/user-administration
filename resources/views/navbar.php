<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">
      <div class="container-fluid">
        <div class="d-flex align-items-center">
          <div class="site-logo mr-auto w-25"><a href="index.php">Users Administration</a></div>

          <div class="mx-auto text-center">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mx-auto d-none d-lg-block  m-0 p-0">
                <li><a href="index.php#home-section" class="nav-link">Home</a></li>
                <li><a href="index.php#courses-section" class="nav-link">Registered Users</a></li>
                <li><a href="index.php#user-section" class="nav-link">Top User</a></li>
              </ul>
            </nav>
          </div>

        <div class="ml-auto w-25">
            <nav class="site-navigation position-relative text-right" role="navigation">
            <ul class="site-menu main-menu site-menu-dark js-clone-nav mr-auto d-none d-lg-block m-0 p-0">
                <?php if (!isset($_SESSION['logged']) && empty($_SESSION['logged'])) {
                    echo '<li class="cta cta_login"><a href="#formLogin" class=""><span>Log in</span></a></li>';
                    echo '<li class="cta"><a href="#contact-section" class=""><span>Register</span></a></li>';
                } else {
                    echo '<li class="cta cta_email">
                        <a href="logout.php" class="nav-link">
                            <span>Log Out</span>
                        </a>
                    </li>';
                }
                ?>
            </ul>
            </nav>
            <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black float-right">
                <span class="icon-menu h3"></span>
            </a>
        </div>
        </div>
    </div>
    </header>

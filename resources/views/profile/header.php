<!DOCTYPE html>
<html lang="en">
  <head>
    <title>User Panel</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" >
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/profile.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" id="home">
    <div class="center-buttons">
        <button type="button" class="btn btn-primary" id="homeButton">Home Page</button>
        <button type="button" class="btn btn-danger" id="exitButton">Exit Profile</button>
    </div>
    <div class="center">

  <div class="card">
    <div class="additional">
      <!-- image left-side -->
      <div class="user-card">
        <div class="level center">
          <button type="button" onclick="location.href='profile_edit.php'" id="edit_btn">Edit</button>
        </div>
        <div class="points center">
          <button type="button" onclick="location.href='profile_delete.php'" id="delete_btn">Delete</button>
        </div>
         <div id="imgHolder" style="display:flex; justify-content: center; align-items: center; padding-top: 50%;">
          <img
            src="<?php echo ($user_exist_db['image'] != '' && file_exists($user_exist_db['image']) ?
            $user_exist_db['image'] :
            'public/images/profile.png') ?>"
            alt="FAIL"
            style="background-color: yellow;width:110px; height:110px; border-radius:40%;"
          >
         </div>
      </div>
      <div class="more-info">
        <h1>
            <?php echo($user_exist_db['firstName'] != '' ?
            'Welcome, ' . $user_exist_db['firstName'] :
            'Enter First Name...'); ?>
        </h1>
         <div class="coords">
          <p>
            <strong>Last Profile Update: </strong>
            <span class="profile_span">
                <?php echo ($user_exist_db['updated_at'] != '' ?
                $user_exist_db['updated_at'] :
                'Unknown Time...') ?>
            </span>
          </p>
          <p>
            <strong>Email:</strong>
            <span class="profile_span">
                <?php echo($user_exist_db['email'] != '' ?
                $user_exist_db['email'] :
                'Enter email...'); ?>
          </p>
          <p>
            <strong>First Name: </strong>
            <span class="profile_span">
                <?php echo($user_exist_db['firstName'] != '' ?
                $user_exist_db['firstName'] :
                'Enter first name...'); ?>
            </p>
          <p>
            <strong>Last Name: </strong>
            <span class="profile_span">
                <?php echo($user_exist_db['lastName'] != '' ? $user_exist_db['lastName'] : 'Enter last name...'); ?>
            </span>
          </p>
        </div>
      </div>

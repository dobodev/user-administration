
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Edit Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" >

    <link rel="stylesheet" href="public/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/profile_edit.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" id="home">
    <div class="center-buttons">
        <button type="button" class="btn btn-primary" id="homeButton">Home Page</button>
        <button type="button" class="btn btn-danger" id="exitButton">Exit Profile</button>
    </div>
    <div class="center">

  <div class="card">
    <div class="additional">
      <!-- image left-side -->
      <div class="user-card">
        <div class="level center">
          <button type="button" onclick="location.href='profile.php'" id="back_btn">Back</button>
        </div>
        <div class="points center">
          <button type="button" id="save_btn">Save</button>
        </div>
         <div id="imgHolder" style="display:flex; justify-content: center; align-items: center; padding-top: 50%;">
            <img src="<?php echo ($user_exist['image'] != '' && file_exists($user_exist['image']) ?
                $user_exist['image'] :
                'images/users/default.png') ?>"
                alt="FAIL"
                style="background-color: yellow;width:110px; height:110px; border-radius:40%;"
            >
         </div>
      </div>
      <!-- right side -->
      <div class="more-info">
        <h1 class="editable" id="email_edit">
            <?php echo($user_exist['email'] != '' ? $user_exist['email'] : 'Enter email...'); ?>
        </h1>
        <div class="coords">
          <span>First Name</span>
          <span contenteditable="true" class="editable" id="firstname_edit">
            <?php echo($user_exist['firstName'] != '' ? $user_exist['firstName'] : 'Enter First Name...'); ?>
          </span>
        </div>
        <div class="coords">
          <span>Last Name</span>
          <span contenteditable="true" class="editable" id="lastname_edit">
                <?php echo($user_exist['lastName'] != '' ? $user_exist['lastName'] : 'Enter Last Name...' ) ?>
          </span>
        </div>
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <input type="file" id="fileToUpload" name="fileToUpload">
            <input type="submit" value="Upload Image" name="submit">
        </form>
         <span id="message" style="color:white;font-weight:bold"></span>
      </div>
    </div>

    <div class="general">
      <h1><?php echo($user_exist['firstName'] != '' ? $user_exist['firstName'] . "'s Profile" : 'Enter name...') ?></h1>
      <p>
        <strong>Email:</strong>
            <span class="profile_span">
                <?php echo($user_exist['email'] != '' ? $user_exist['email'] : 'Enter email...'); ?>
            </span>
      </p>
      <p>
        <strong>First Name: </strong>
            <span class="profile_span">
                <?php echo($user_exist['firstName'] != '' ? $user_exist['firstName'] : 'Enter first name...'); ?>
            </span>
      </p>
      <p>
        <strong>Last Name: </strong>
            <span class="profile_span">
                <?php echo($user_exist['lastName'] != '' ? $user_exist['lastName'] : 'Enter last name...'); ?>
            </span>
      </p>
      <span class="more">`Hover to edit...</span>
    </div>
  </div>


<div class="intro-section" id="home-section">
  <div class="slide-1" style="background-image: url('/public/images/home.jpg');" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="row align-items-center">
            <div class="col-lg-6 mb-4">
              <?php
                if (!isset($_SESSION['logged']) && empty($_SESSION['logged'])) {
                    echo '<h1  data-aos="fade-up" data-aos-delay="100">Register here</h1>
                    <p class="mb-4"  data-aos="fade-up" data-aos-delay="200">Join our forces...</p>
                    <p data-aos="fade-up" data-aos-delay="300">
                    <a href="#contact-section" class="btn btn-primary py-3 px-5 btn-pill">Register</a>
                </p>';
                } else {
                    echo '<h1  data-aos="fade-up" data-aos-delay="100">Hello, ' . $_SESSION['firstName'] . '</h1>
              ';
                }
                ?>
            </div>

            <?php
            if (!isset($_SESSION['logged']) && empty($_SESSION['logged'])) {
                echo '<div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="500">
              <form action="logged.php" method="post" class="form-box" id="formLogin">
                <h5 id="login_info"></h5>
                <h3 class="h4 text-black mb-4">Sign In</h3>
                <span id="pass_reset" style="color:red;display:none;">Password Changed, please login</span>
                <div class="form-group">
                    <input type="email"
                        class="form-control"
                        placeholder="Email"
                        id="email_login"
                        name="email_login"
                        required
                    >
                </div>
                <div class="form-group">
                    <input type="password"
                        class="form-control"
                        placeholder="Password"
                        id="password_login"
                        name="password_login"
                        required
                    >
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-primary btn-pill" value="Sign up" id="submit">
                    <a href="forgot_password.php" class="btn btn-link" value="Forgot password" id="forgot_pass">
                        Forgot password
                    </a>
                </div>

              </form>
            </div>';
            } else {
                $user_img = $user_exist_db["image"] != '' && file_exists($user_exist_db["image"]) ?
                    $user_exist_db["image"] :
                    "/public/images/users/default.png";
                echo'<div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="500" style="display: table;">
                <a href="profile.php">
                    <img src="' . $user_img . '"
                    alt="Profile img"
                    style="display: table-cell; width: 100%; border-radius: 10%;"
                >
                </a>
                </div>';
            }
            ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="site-section users-title" id="courses-section">
  <div class="container">
    <div class="row mb-5 justify-content-center">
      <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="">
        <h2 class="section-title">Registered Users</h2>
      </div>
    </div>
  </div>
</div>

<div class="site-section courses-entry-wrap"  data-aos="fade-up" data-aos-delay="100">
  <div class="container">
    <div class="row">
      <div class="owl-carousel col-12 nonloop-block-14">
          <?php foreach ($all_users as $user) { ?>
            <div class="course bg-white h-100 align-self-stretch user-profile">
              <figure class="m-0">
                <a href="course-single.php?character=<?php echo $user['firstName']; ?>">
                    <img src="<?php echo ($user["image"] != '' && file_exists($user['image']) ?
                        $user['image'] :
                        '/public/images/users/default.png' ); ?>
                        "
                    alt="Image Failed"
                    class="img-fluid"
                    >
                </a>
              </figure>
              <div class="course-inner-text py-4 px-4">
                <div class="meta">
                    <span class="fa fa-user-o"></span>
                    <span id="user_name">
                        <?php echo $user["firstName"]; ?>
                    </span>
                </div>
                <!-- <h3><a href="#">User data</a></h3> -->
                <!-- <p>User data</p> -->
              </div>
              <div class="d-flex border-top stats">
                <i class="fa-like fa fa-thumbs-up liker"></i>
                <div class="py-3 px-4">
                    <span class="icon-users"></span>
                    <span id="num_likes" style="font: italic bold 26px Georgia, serif; color: #009973;">
                        <?php echo "\t" . $user['likes'];?>
                    </span> like(s)
                </div>
                <!-- <div class="py-3 px-4 w-25 ml-auto border-left"><span class="icon-chat"></span> 2</div> -->
              </div>
            </div>
          <?php } ?>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-7 text-center">
        <button class="customPrevBtn btn btn-primary m-1">Prev</button>
        <button class="customNextBtn btn btn-primary m-1">Next</button>
      </div>
    </div>

  </div>
</div>

<div class="site-section bg-image overlay"
    id="user-section"
    style="background-image: url('/public/images/top_usr_bgr.jpg');"
>
  <div class="container">
    <div class="row justify-content-center align-items-center">
      <div class="col-md-8 text-center testimony">
        <!-- TODO: update changes top-user without reload -->
        <img src="<?php echo ($top_user[0]["image"] != '' && file_exists($top_user[0]['image']) ?
            $top_user[0]['image'] :
            '/public/images/users/default.png' ); ?>"
            alt="Image"
            class="img-fluid w-25 mb-4 rounded-circle"
        >
        <!-- <h3 class="mb-4">Most Liked User</h3> -->
        <blockquote>
          <p>&ldquo; Current Top User :
            <strong>
                <?php echo ($top_user[0]['firstName'] ? $top_user[0]['firstName'] : 'Unknown'); ?>
            </strong> &rdquo;
        </p>
        </blockquote>
      </div>
    </div>
  </div>
</div>

<?php if (!isset($_SESSION['logged']) && empty($_SESSION['logged'])) {
    if (!isset($_GET['reset_password'])) {
        echo '<div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7">
            <h2 class="section-title mb-3">Registration Form</h2>
            <form action="register.php" method="post" data-aos="fade" id="formRegister">
            <h5 id="register_info"></h5>
              <div class="form-group row">
                <div class="col-md-6 mb-3 mb-lg-0">
                    <input type="text"
                        class="form-control"
                        placeholder="First name"
                        id="firstName"
                        name="firstName"
                        required
                    >
                </div>
                <div class="col-md-6">
                    <input type="text"
                        class="form-control"
                        placeholder="Last name"
                        id="lastName"
                        name="lastName"
                        required
                    >
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <input type="email" class="form-control" placeholder="Email" id="email_reg" name="email_reg" required>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                    <input type="password"
                        class="form-control"
                        placeholder="Password"
                        id="password_reg"
                        name="password_reg"
                        required
                    >
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                    <input type="password"
                        class="form-control"
                        placeholder="Re Password"
                        id="repassword"
                        name="repassword"
                        required
                    >
                </div>
              </div>
                    <span id="message"></span>
              <div class="form-group row">
                <div class="col-md-6">
                    <input type="submit"
                        name="submit"
                        class="btn btn-primary py-3 px-5 btn-block btn-pill"
                        value="Register"
                    >
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>';
    } else {
        $reset_password = $_GET['reset_password'] != '' ? $_GET['reset_password'] : '';
        echo '<div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7">
            <h2 class="section-title mb-3">Reset your password</h2>
            <form method="post" action="reset_password.php" data-aos="fade" id="formReset">
            <h5 id="reset_info"></h5>
              <div class="form-group row">
                <div class="col-md-12">
                    <input type="password"
                        class="form-control"
                        placeholder="New Password"
                        id="resetpassword"
                        name="resetpassword"
                        required
                    >
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                    <input type="password"
                        class="form-control"
                        placeholder="Re-type New Password"
                        id="reresetpassword"
                        name="reresetpassword"
                        required
                    >
                </div>
              </div>
              <span id="message"></span>
              <input type="hidden" name="reset_password" value="' . $reset_password . '">

              <div class="form-group row">
                <div class="col-md-6">
                  <input type="submit" class="btn btn-primary py-3 px-5 btn-block btn-pill" value="Reset password">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>';
    }
}
?>

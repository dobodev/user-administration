<?php

session_start();
session_unset();
session_destroy();
$message = 'logout';
header("Location: index.php?url_action=" . $message);
exit();

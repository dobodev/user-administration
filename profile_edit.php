<?php

require __DIR__ . '/bootstrap.php';

use Classes\Database;

// New db-object
$db = new Database();

$user_email = $_SESSION['email'] ?? 'no email';
$user_firstName = $_SESSION['firstName'] ?? 'no first name';
$user_lastName = $_SESSION['lastName'] ?? 'no last name';
$user_logged = $_SESSION['logged'] ?? 'no logged';

$user_exist = $db->selectUserFromDatabase($user_email);

// Compare the session vs DB record
if ($user_logged != $user_exist['logged'] || $user_logged == null || $user_exist == false) {
    session_unset();
    session_destroy();
    $login_error = 'restricted';
    header("Location: index.php?url_action=" . $login_error . '#home-section');
    die('Unauthorized access');
}
?>

<?php require_once 'resources/views/profile/edit/header.php'; ?>

  <!-- scripts -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script>
   $(document).ready(function() {
    var get_msg = '<?php echo (isset($_GET['msg']) ? $_GET['msg'] : ''); ?>';

    if(get_msg == 'img_exist'){
      $('#message').text('Sorry, file with same name already exists!');
    }else if(get_msg == 'img_size_err'){
      $('#message').text('Sorry, only valid files up to 2MB for image is allowed!');
    }else if(get_msg == 'img_type_err'){
      $('#message').text('Sorry, only JPG, JPEG, PNG & GIF files are allowed!');
    }else if(get_msg == 'img_success'){
      $('#message').text('Your profile picture was updated!');
    }else if(get_msg == 'img_fail'){
      $('#message').text('Sorry, there was an error uploading the file on server!');
    }else{
      $('#message').text('');
    }

    //Home
    $('#homeButton').on('click', function(){
        window.location.href= "index.php";
    });

    //Exit
    $('#exitButton').on('click', function(){
        window.location.href= "logout.php";
    });

    // save data
    $('#save_btn').on('click', function(e) {
        var firstName_input = $('#firstname_edit').text();
        var lastName_input = $('#lastname_edit').text();
        $.ajax({
        data: {
            firstName: firstName_input,
            lastName: lastName_input
        },
        method: "POST",
        url: "profile_edit_save_db.php",
        success: function(msg){
            $('#message').text('Profile Updated');
        },
        error: function(msg){
            $('#message').text('Error occured, unable to update.');
        }
        });//ajax
    });//on click save_btn
   });
  </script>

  </body>
</html>

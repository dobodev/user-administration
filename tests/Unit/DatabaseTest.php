<?php

declare(strict_types=1);

namespace Tests\Unit;

// require_once __DIR__ . '/../config/bootstrap.php';

use Classes\Database;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;

class DatabaseTest extends TestCase
{
    private $databaseMocker;
    private $databaseStub;

    public function setUp(): void
    {
        $this->databaseMocker = $this->createMock(Database::class);
        $this->databaseStub = $this->createStub(Database::class);
    }

    public function tearDown(): void
    {
        unset($this->databaseMocker);
        unset($this->databaseStub);
    }

    /**
     * Test select user from database
     */
    #[DataProvider('validEmails')]
    public function testSelectUserFromDatabase(string $email, bool $result): void
    {
        $this->databaseMocker
            ->expects($this->once())
            ->method('selectUserFromDatabase')
            ->with($email)
            ->willReturn([
                'id' => rand(1, 100),
                'email' => $email,
            ]);

        $result = $this->databaseMocker->selectUserFromDatabase($email);

        $this->assertEquals($result['email'], $email);
    }

    /**
     * Dataprovider for valid emails
     *
     */
    public static function validEmails(): array
    {
        return [
            ['admin@gmail.com', true],
            ['user1@gmail.com', true],
            ['user2@gmail.com', true],
        ];
    }

    public function testCheckUserLogged(): void
    {
        $this->databaseMocker
            ->expects($this->once())
            ->method('checkUserLogged')
            ->with('admin@gmail.com', 121212)
            ->willReturn([
                'logged' => '121212',
                'email' => 'admin@gmail.com',
            ]);

        $result = $this->databaseMocker->checkUserLogged('admin@gmail.com', '121212');

        $this->assertEquals($result['email'], 'admin@gmail.com');
        $this->assertEquals($result['logged'], 121212);
    }

    public function testEmailExist(): void
    {
        $this->databaseMocker
            ->expects($this->once())
            ->method('emailExist')
            ->with('admin@gmail.com')
            ->willReturn(true);

        $result = $this->databaseMocker->emailExist('admin@gmail.com');

        $this->assertTrue($result);
    }

    public function testInsertUserDatabase(): void
    {
        $this->databaseMocker
            ->expects($this->once())
            ->method('insertUserDatabase')
            ->with('admin', 'admin', 'admin@gmail.com', 'pass')
            ->willReturn(['admin', 'admin', 'admin@gmail.com', 'pass']);

        $result = $this->databaseMocker->insertUserDatabase('admin', 'admin', 'admin@gmail.com', 'pass');

        $this->assertEquals($result, ['admin', 'admin', 'admin@gmail.com', 'pass']);
    }

    public function testDeleteUserDatabase(): void
    {
        $this->databaseMocker
            ->expects($this->once())
            ->method('deleteUserDatabase')
            ->with('admin@gmail.com')
            ->willReturn(1);

        $result = $this->databaseMocker->deleteUserDatabase('admin@gmail.com');

        $this->assertEquals($result, 1);
    }
}

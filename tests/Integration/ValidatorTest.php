<?php

namespace Tests\Integration;

use Classes\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    private $validator;
    public function setUp(): void
    {
        $this->validator = new Validator();
    }

    public function tearDown(): void
    {
        unset($this->validator);
    }

    public function testValidateEmail(): void
    {
        $this->assertEquals($this->validator->validateEmail('admin@gmail.com'), 'admin@gmail.com');

        $this->assertFalse($this->validator->validateEmail('admin@1'));
    }

    public function testTrim(): void
    {
        $this->assertEquals($this->validator->trim('admin@gmail'), 'admin@gmail');

        $this->assertEquals($this->validator->trim('     admin@gmail.com'), 'admin@gmail.com');

        $this->assertSame($this->validator->trim('""admin@gmail.com     '), '&quot;&quot;admin@gmail.com');
    }
}

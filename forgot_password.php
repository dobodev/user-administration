<?php require_once 'resources/views/forgot-password/header.php'; ?>

    <div id="forgotten-container">
        <h1>Forgot Password ?</h1>
        <form method="POST" action="reset_email.php">
        <!-- <span id="msg"><?php // echo $_GET['error'] == 'mailNotFound' ? 'Email not found' : ''; ?></span> -->
            <input type="email" name="email" placeholder="E-mail" required>
            <a href="index.php" class="green-btn">Home</a>
            <!-- <a href="submit" class="red-btn">Get new password</a> -->
            <input type="submit" class="btn btn-primary py-3 px-5 btn-block btn-pill red-btn" value="Reset password">
        </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" >
        $('#login-button').click(function(){
            $('#login-button').fadeOut("slow",function(){
            $("#forgotten-container").fadeIn();
            TweenMax.from("#forgotten-container", .4, { scale: 0, ease:Sine.easeInOut});
            TweenMax.to("#forgotten-container", .4, { scale: 1, ease:Sine.easeInOut});
            });
        });
    </script>
</body>
</html>

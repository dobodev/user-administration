-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: users_administration
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_counter`
--

DROP TABLE IF EXISTS `user_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_counter` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_liked` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_counter_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_counter`
--

LOCK TABLES `user_counter` WRITE;
/*!40000 ALTER TABLE `user_counter` DISABLE KEYS */;
INSERT INTO `user_counter` VALUES (114,'Jenna',4,'2020-12-22 18:06:19'),(118,'Admin',7,'2020-12-29 22:43:51'),(138,'Admin',8,'2020-12-29 23:24:03'),(139,'Admin',1,'2020-12-30 22:25:18'),(145,'Anna',9,'2020-12-31 18:17:11');
/*!40000 ALTER TABLE `user_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `logged` varchar(50) DEFAULT NULL,
  `password` varchar(99) NOT NULL,
  `reset_string` varchar(55) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','Admin','admin@gmail.com','1609768820.2605_admin@gmail.com','$2y$10$3q0OOnpxNphbin80HL6GvuuOyzBjAuNhGkIq6pvY6oOu/CwiBd7jC',NULL,'images/users/administrator.jpg','2020-06-10 13:16:30','2020-12-29 09:07:42'),(4,'Jimmy','Rivera','user1@gmail.com','1594485303.7265_user1@gmail.com','$2y$10$zuU830fWAY7enRopd.lLQOGN/wiWLpDRgmT2jPpRhHs1SKamu3Kuu','lmm.ros!$%ceigau11594569872','images/users/jimmy.jpg','2020-06-10 13:16:30','2020-06-17 11:35:47'),(5,'Anna','Maria','user2@gmail.com','1592312061.0413_user@gmail.com','$2y$10$3q0OOnpxNphbin80HL6GvuuOyzBjAuNhGkIq6pvY6oOu/CwiBd7jC',NULL,'images/users/anna.jpg','2020-06-10 13:16:30','2020-06-10 13:16:30'),(7,'Jenna','Smith','jenna@gmail.com','1609281817.8346_jenna@gmail.com','$2y$10$UcV8XDhKCsexGR0dRl7a6eqySstndFjuGJ0cx/vjWtMX.g.F4do36',NULL,'images/users/jenna.jpg','2020-07-18 18:16:49','2020-07-18 18:16:49'),(8,'George','Brimas','george@gmail.com','1609340506.6966_george@gmail.com','$2y$10$JTiKNBMG5i2e.eBdeEhNC./VSKyvPiBUCTd.2ek5hwh/J9QZjDK4a','eacgooemi!$%gmglr.1609340525','images/users/george.jpg','2020-12-30 01:17:52','2020-12-30 01:57:47'),(9,'Hector','Bronson','hector@gmail.com','1609438210.0967_hector@gmail.com','$2y$10$mAWxMbYlnZJx6DKbOb9ltuta3LQ6yG1GF8nvz0daLkKaIOgXK7rPy','m!$%.imeahocrgtloc1609438033','images/users/hector.jpg','2020-12-31 19:54:18','2020-12-31 08:11:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-11 10:08:57

$(document).ready(function () {
// var helpMsg = '<?php echo isset($help_msg) ? $help_msg : ''; ?>';  // get the $_GET url-param
// console.log(sessionStorage);
//add the text to the empty input-field for the error message
    if (typeof helpMsg !== 'undefined') {
        switch (helpMsg) {
            case 'There is a registration with that email':
                $('#register_info').text(helpMsg);
            break;

            case 'Message sent':
                $('#message_info').text(helpMsg);
            break;

            case 'Successfully logged out':
                $('#pass_reset').css('display', 'block');
                $('#pass_reset').text(helpMsg);
                sessionStorage.clear();
            break;

            case 'password_changed':
                $('#pass_reset').css('display', 'block');
            break;

            default:
                $('#login_info').text(helpMsg);
            break;
        }
    }

// var email_login_session = (sessionStorage.getItem('email_login') != 'undefined') ?
//     sessionStorage.getItem('email_login') :
//     '';
// var firstName_session = (sessionStorage.getItem('firstName') != 'undefined') ?
//     sessionStorage.getItem('firstName') :
//     '';
// var lastName_session = (sessionStorage.getItem('lastName') != 'undefined') ?
//     sessionStorage.getItem('lastName') :
//     '';
// var email_session = (sessionStorage.getItem('email_reg') != 'undefined') ?
//     sessionStorage.getItem('email_reg') :
// '';

//Retype the 2-forms-fields with the sessionStorage
    $('#email_login').val(email_login_session);
    $('#firstName').val(firstName_session);
    $('#lastName').val(lastName_session);
    $('#email_reg').val(email_session);

//save the send-email-field-data for the session on input-change
    $('#email_login, #firstName, #lastName, #subject, #email_reg, #textarea').on('change', function () {
        sessionStorage.setItem('email_login', $('#email_login').val());
        sessionStorage.setItem('firstName', $('#firstName').val());
        sessionStorage.setItem('lastName', $('#lastName').val());
        sessionStorage.setItem('email_reg', $('#email_reg').val());
    });

// Function for comparing pass/repass, set msg, set reg_check for form to enable
    function passwordCompare(pass, repass)
    {
        var pass_check = false;

        if (pass == repass) {
            if (pass.length < 6) {
                $('#message').html('Password must be at least 6 characters').css('color', 'red');
                pass_check = false;
            } else {
                $('#message').html('').css('color', '');
                pass_check = true;
            }
        } else {
            pass_check = false;
            $('#message').html('Passwords don\'t match').css('color', 'red');
        }

        return pass_check;
    }

// Register Form
    var register_pass_check = false;
    $('#password_reg, #repassword').on('keyup', function () {
        var check = passwordCompare($('#password_reg').val(), $('#repassword').val());

        register_pass_check = check;
    });
    $('#formRegister').submit(function (event) {
        if (register_pass_check === false) {
            event.preventDefault();
        }
    });

// Reset pass form
    var reset_pass_check = false;
    $('#resetpassword, #reresetpassword').on('keyup', function () {
        var check = passwordCompare($('#resetpassword').val(), $('#reresetpassword').val());
        reset_pass_check = check;
    });
    $('#formReset').submit(function (event) {
        if (reset_pass_check == false) {
            event.preventDefault();
        }
    });


// Sesssion user data
// var logged = '<?php //echo ( isset($logged_user) ? json_encode($logged_user) : ''); ?>';

    if (logged != '') {
        var logged_arr = JSON.parse(logged);
    }


    //TODO:  Detect logged user has already liked someone, and highlight the thumb/counter
    // if (logged_arr && logged_arr.length !== 0) {
    //     var user_id = logged_arr[0].id;

    //     // Extract the liked data from the DB
    //     $.ajax({
    //         method: "POST",
    //         url: "ratings.php" ,
    //         data:{
    //             logged_user_id: user_id
    //         },
    //         success: function (response) {
    //             var name_liked = response; // the name returned from the db query

    //             if (name_liked) {
    //                 // set the active class for the db-liked name
    //                 $("span:contains('" + name_liked + "')").parent().parent().next().find('.liker').addClass('fa-active');
    //             }
    //         }
    //     });
    // }

// Like/Dislike functionality
    $(".container").on('click', '.liker', function () {
        // console.log(logged_arr[0].email);
        if (logged_arr.length == 0 || logged_arr == undefined) {
        // alert('Only registered users allowed to vote');
            bootbox.alert({
                message: "Only registered users are allowed to vote!",
                size: 'small',
                className: 'rubberBand animated'
            });
            return;
        }

        var logged_user_id = logged_arr[0].id;

        var clickedBtn = $(this);
        var currentClickedName = clickedBtn.closest(".owl-item").find("#user_name").text();
        var currentClickedLikes = parseInt(clickedBtn.next().find('#num_likes').text());

        if (clickedBtn.hasClass('fa-active')) {
            action = 'dislike';
        } else if (!clickedBtn.hasClass('fa-active')) {
            action = 'like';
        }

        // AJAX request, to save the data
        $.ajax({
            method: "POST",
            url: "ratings.php",
            data:{
                name: currentClickedName,
                visitor_id: logged_user_id,
                action: action
            },
            success: function (data) {
                if (action == 'like') {
                    if (data == 'liked') {
                        // reduce the current liked-user count
                        var currentLikedCount = parseInt($('i.fa-active').next().find('#num_likes').first().text());
                        var reducer = currentLikedCount - 1;
                        var currentLikedName = $('i.fa-active').parent().parent().find('#user_name').first().text();
                        $('span:contains(' + currentLikedName + ')').parent().parent().next().find('#num_likes').text(reducer);

                        // increment the clicked user count
                        var incr = currentClickedLikes + 1;
                        $('.fa-like').removeClass('fa-active');
                        clickedBtn.addClass('fa-active');
                        clickedBtn.next().find('#num_likes').text(incr);
                        // alert("You liked" + ": " +  currentClickedName);
                        bootbox.alert({
                            message: "You liked" + ": " +  currentClickedName,
                            size: 'small',
                            className: 'rubberBand animated'
                        });
                    }
                } else if (action == 'dislike') {
                    if (data == 'unliked') {
                        var decr = currentClickedLikes - 1;
                        clickedBtn.removeClass('fa-active');
                        clickedBtn.next().find('#num_likes').text(decr);
                    // alert("You disLiked" + ": " +  currentClickedName);
                        bootbox.alert({
                            message: "You disLiked" + ": " +  currentClickedName,
                            size: 'small',
                            className: 'rubberBand animated'
                        });
                    }
                }
            }
        });
    });

});//document ready

<?php

require __DIR__ . '/config/bootstrap.php';

use Classes\Database;

$data = $_POST;
$action = $data['action'] ?? '';
$logged_user_id = $data['logged_user_id'] ?? '';

$db = new Database();

if (isset($action)) {
    switch ($action) {
        case 'like':
            $result =  $db->likeUser($data['name'], $data['visitor_id']);
            if ($result == 1) {
                echo 'liked';
            }
            break;
        case 'dislike':
            $result = $db->unLikeUser($data['name'], $data['visitor_id']);
            if ($result == 1) {
                echo 'unliked';
            }
            break;
        default:
            // http_response_code('HTTP/1.1 400 Bad Request');
            header('Location: index.php?action=missing_rating');
            exit();
            break;
    }
} elseif (isset($logged_user_id)) {
    $result = $db->checkUserReaction($logged_user_id);

    echo $result['user_liked'];
}

// http_response_code('HTTP/1.1 400 Bad Request');
// header('Location: index.php?action=wrong_rating');
